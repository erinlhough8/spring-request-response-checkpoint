package com.example;

import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
public class WordsController {

    @GetMapping("/camelize")
    public String camelize(@RequestParam String original, @RequestParam(defaultValue = "false") boolean initialCap) {
        String[] words = original.split("_");

        String first = words[0];
        if (initialCap) first = first.substring(0,1).toUpperCase() + first.substring(1).toLowerCase();

        return first + IntStream.range(0, words.length)
                .filter(i -> i > 0)
                .mapToObj(i -> words[i].substring(0,1).toUpperCase() + words[i].substring(1).toLowerCase())
                .collect(Collectors.joining(""));
    }

    @GetMapping("/redact")
    public String redact(@RequestParam String original, @RequestParam List<String> badWord) {
        return Arrays.stream(original.split(" ")).map(word -> {
            if (badWord.contains(word)) {
                return IntStream.range(0, word.length()).mapToObj(i -> "*").collect(Collectors.joining());
            }
            return word;
        }).collect(Collectors.joining(" "));
    }

    @PostMapping("/encode")
    public String redact(@RequestParam String message, @RequestParam String key) {
        String alphabet = "abcdefghijklmnopqrstuvwzyz";
        return Arrays.stream(message.split("")).map(letter -> {
            if (letter.equals(" ")) return letter;
            return String.valueOf(key.charAt(alphabet.indexOf(letter)));
        }).collect(Collectors.joining());
    }

    @PostMapping("/s/{find}/{replace}")
    public String sed(@PathVariable String find, @PathVariable String replace, @RequestBody String document) {
        return document.replace(find, replace);
    }

}
